#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if(tail==0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
    Node *tmp = head;
    while (tmp && tmp->next != NULL) {
        tmp = tmp->next;
    }
    tail = new Node(el);
    tail->next = NULL;
    if (tmp == NULL)
        head = tail;
    else
        tmp->next = tail;
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{

    if(head == tail)
    {
        delete head;
        head = tail =0;
    }
    else
    {
        Node *tmp;
        for(tmp=head; tmp->next != tail ; tmp = tmp->next);
        delete tail;
        tail = tmp;
        tail->next = 0;
    }
    return NULL;
}
bool List::search(char el)
{
	// TO DO! (Function to return True or False depending if a character is in the list.

        Node *tmp;
        tmp = head;
        while (tmp->data != el && tmp->next != NULL) {
            tmp= tmp->next;
        }
        if (tmp->data != el && tmp->next == NULL) {
            return false;
        }
        else
            return true;
}

void List::reverse()
{
	// TO DO! (Function is to reverse the order of elements in the list.
 
    Node *temp = head;
    Node *temp2 = NULL;
    Node *hold = NULL;
    tail = head;
    temp2 = head->next;
    while (temp2 != NULL) {
        hold = temp2;
        temp2 = temp2->next;
        hold->next = temp;
        temp = hold;
    }
    head = temp;
    tail->next = NULL;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}